# expected env variables
# HOST=localhost
# USERNAME=postgres
# DBNAME=dev_bubble_racing
# BUBBLE_RACING_API_PASSWORD=
# PGPASSWORD=

# exit when any command fails
set -e

# set working directory to script location
cd "$(dirname "$0")"

# create short psql command
PSQL="psql --host=${HOST} --username=${USERNAME} --dbname=${DBNAME}"

# create role
sed -e "s/\${PASSWORD}/${BUBBLE_RACING_API_PASSWORD}/g" \
    -e "s/\${ROLE}/${DBNAME}_api/g" \
    -e "s/\${DBNAME}/${DBNAME}/g" \
    ./create-role.sql.tpl > /tmp/create-role.sql
echo "${PSQL} --file=/tmp/create-role.sql"
${PSQL} --file=/tmp/create-role.sql