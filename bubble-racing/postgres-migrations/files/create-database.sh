# exit when any command fails
set -e

# create database if it doesn't exist
PSQL="psql --host=${HOST} --username=${USERNAME} --dbname=${DBNAME}"
${PSQL} --dbname=postgres --command="CREATE DATABASE ${DBNAME}" || echo 'database already exists'