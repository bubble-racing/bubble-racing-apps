CREATE TABLE IF NOT EXISTS gameservers (
    gameserver_id INTEGER PRIMARY KEY,
    address VARCHAR (50) NOT NULL,
    port INTEGER NOT NULL,
    player_count INTEGER NOT NULL,
    is_open BOOLEAN NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL
);
