# exit on error
set -e

# prepare postgres password
echo "${HOST}:5432:${DBNAME}:${USERNAME}:${PGPASSWORD}" > ~/.pgpass
chmod 600 ~/.pgpass

# run migration
database="postgres://${USERNAME}@${HOST}:5432/${DBNAME}?sslmode=disable"
migrate -database $database -path $MIGRATIONS_PATH $DIRECTION $COUNT