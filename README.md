### Bubble Racing Apps

# Connect to grafana
```
kubectl get secret --namespace global kube-prometheus-stack-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
kubectl port-forward service/kube-prometheus-stack-grafana 8080:80 --namespace global
user name is admin
```

# Connect to postgres
```
# This stored secret is only correct the first time the chart is deployed. Any subsequent deployments will store a new (and wrong) random password in the secret
kubectl get secret --namespace apps postgres-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode ; echo
kubectl port-forward service/postgres-postgresql 5432:5432 --namespace dev-bubble-racing
psql --host=localhost --username=postgres --dbname=postgres
```

# Run locally
```
docker run -it --entrypoint '' --volume $PWD:/code --volume ~/.kube:/root/.kube chatwork/helmfile:0.143.1-3.8.1 bash
cd code/bubble-racing
export CI_COMMIT_BRANCH=dev
export POSTGRES_PASSWORD=
export BUCKET_ACCESS_KEY=
export BUCKET_SECRET_KEY=
export GAMESERVER_KEY=
helmfile --environment $CI_COMMIT_BRANCH --file ./bubble-racing-helmfile.yaml apply --suppress-secrets
```
