#!/bin/bash -e

set -x

export HELM_NAMESPACE=$1

# Ensure namespace
echo "Ensuring namespace '$HELM_NAMESPACE' exists..."
if ! kubectl get namespace "$HELM_NAMESPACE" > /dev/null 2>&1; then
  kubectl create namespace "$HELM_NAMESPACE"
fi